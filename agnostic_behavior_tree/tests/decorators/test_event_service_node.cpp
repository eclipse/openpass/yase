/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "agnostic_behavior_tree/actions/analyse_nodes.h"
#include "agnostic_behavior_tree/actions/functor_action_node.h"
#include "agnostic_behavior_tree/composite/parallel_node.h"
#include "agnostic_behavior_tree/decorator/service_node.h"
#include "agnostic_behavior_tree/utils/visitors.h"

#include <gtest/gtest.h>
#include <vector>

namespace yase {

// Test if child is initialised and terminated
TEST(ServiceTest, test_onInit_and_onTerminate) {
  class DummyService : public ServiceNode::Service {
   public:
    void lookupAndRegisterData(Blackboard& /*blackboard*/) final{};
    void preUpdate() final{};
  };

  // Setup
  std::shared_ptr<AnalyseNode> dummy_node_1 = std::make_shared<AnalyseNode>(1);

  DecoratorNode::Ptr test_decorator =
      std::make_shared<ServiceNode>("CollisionDetector", std::make_unique<DummyService>());
  test_decorator->setChild(dummy_node_1);

  // Init and tick of parallel (should init children as well)
  test_decorator->onInit();
  EXPECT_EQ(dummy_node_1->isInitialised(), true);
  test_decorator->onTerminate();
  EXPECT_EQ(dummy_node_1->isInitialised(), false);
}

// Exemplary test, which detects a CollisionEvent
//
// The collisions are observed and a node downstream react on it.
TEST(ServiceTest, event_service_node_1) {
  // ------------- TEST INSTANCES -----------------

  // Exemplary collision information
  struct Collisions {
    bool ego_involved{false};
    std::vector<unsigned int> collided_vehicles{};
  };

  // Exemplary CollisionEvent
  //
  // "Determines" collisions within the world and adds the event to the declared struct.
  // After three ticks the Updater "determines" a collision
  class CollisionEvent : public ServiceNode::Service {
   public:
    // Get shared symbol
    void lookupAndRegisterData(Blackboard& blackboard) final {
      m_collisions = std::make_shared<Collisions>();
      blackboard.set<std::shared_ptr<Collisions>>("vehicle_collisions", m_collisions);
    };
    // After three ticks there should be a collision
    void preUpdate() final {
      if (m_collisions) {
      }
      if (m_count == 2) {
        m_collisions->ego_involved = true;
        m_collisions->collided_vehicles.push_back(1);
        m_collisions->collided_vehicles.push_back(2);
      } else {
        m_count++;
      }
    };

   private:
    // After the init this is accessible in the symbol table and all sub nodes which have access
    std::shared_ptr<Collisions> m_collisions{nullptr};
    unsigned int m_count{0};
  };

  // ------------- TESTING -----------------
  // The Event observer service
  ServiceNode::Service::UPtr collsion_event = std::make_unique<CollisionEvent>();
  DecoratorNode::Ptr collsion_event_decorator =
      std::make_shared<ServiceNode>("CollisionDetector", std::move(collsion_event));

  // A node which needs to react on event
  class DriveWithoutCollision : public ActionNode {
   public:
    DriveWithoutCollision() : ActionNode("DriveWithoutCollision"){};

    void onInit() override{};

   private:
    // Reacts on a collision
    NodeStatus tick() final {
      if (m_collisions->ego_involved == true) {
        // Collision observed
        return NodeStatus::kFailure;
      }
      // normal driving
      return NodeStatus::kRunning;
    };

    // Look up required symbol "vehicle_collisions"
    void lookupAndRegisterData(Blackboard& blackboard) final {
      m_collisions = blackboard.get<std::shared_ptr<Collisions>>("vehicle_collisions");
    };

    std::shared_ptr<Collisions> m_collisions{};
  };

  // sub test tree - parallel one child is always successful, while the other waits for collision
  CompositeNode::Ptr parallel = std::make_shared<ParallelNode>();
  collsion_event_decorator->setChild(parallel);
  parallel->addChild(std::make_shared<AlwaysSuccess>());
  parallel->addChild(std::make_shared<DriveWithoutCollision>());

  // The the collision will be observed by the service after three ticks, which causes a failure downstream:
  collsion_event_decorator->distributeData();
  EXPECT_EQ(collsion_event_decorator->executeTick(), NodeStatus::kRunning);
  EXPECT_EQ(collsion_event_decorator->executeTick(), NodeStatus::kRunning);
  EXPECT_EQ(collsion_event_decorator->executeTick(), NodeStatus::kFailure);
}

}  // namespace yase
