/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_BEHAVIOR_TREE_DECORATOR_DATA_PROXY_NODE_H
#define AGNOSTIC_BEHAVIOR_TREE_DECORATOR_DATA_PROXY_NODE_H

#include "agnostic_behavior_tree/decorator/data_declaration_node.h"
#include "agnostic_behavior_tree/decorator_node.h"
#include "agnostic_behavior_tree/utils/dll_export_helper.h"

namespace yase {
// The ProxySetting interface
//
// One can inherit from this class to create a specific proxy settings and declare additional local data
class ABT_DLL_EXPORT ProxySetting {
 public:
  using UPtr = std::unique_ptr<ProxySetting>;

  ProxySetting() = default;
  virtual ~ProxySetting() = default;

  // Provides a proxy table for keys and allows to declare additional local data.
  virtual Blackboard::ProxyTable createProxyTableAndDeclareData(Blackboard& /*blackboard*/);
};

// The DataProxyNode
//
// Allows to proxy data of the parents and add additional local data.
class ABT_DLL_EXPORT DataProxyNode : public DecoratorNode {
 public:
  // Ctor for proxy argument
  DataProxyNode(const std::string& name, ProxySetting::UPtr proxy_setting, Extension::UPtr extension_ptr = nullptr);

  virtual ~DataProxyNode() override = default;

 private:
  // Ticks the decorated child node directly
  NodeStatus tick() override;

  // Declare local data and set proxy settings
  void lookupAndRegisterData(Blackboard& blackboard) final;

  // The managed proxy_setting
  const ProxySetting::UPtr m_proxy_setting;
};

}  // namespace yase

#endif  // AGNOSTIC_BEHAVIOR_TREE_DECORATOR_DATA_PROXY_NODE_H
