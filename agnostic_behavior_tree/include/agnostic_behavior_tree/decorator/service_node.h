/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_BEHAVIOR_TREE_DECORATOR_SERVICE_NODE_H
#define AGNOSTIC_BEHAVIOR_TREE_DECORATOR_SERVICE_NODE_H

#include "agnostic_behavior_tree/decorator_node.h"
#include "agnostic_behavior_tree/utils/dll_export_helper.h"

#include <vector>

namespace yase {

/// \brief Provides one or more services accessible for the subtree.
///
/// Services are updated before and after the tick of the decoratorated child.
///
/// Examples:
/// 1) Event evaluation such as "Vehicle is closer than 10 m to ego" for all child nodes
/// 2) Calculate utils services such as "Calculate lane association for all vehicles" once for all sub nodes
/// 3) Integration of co simulators
class ABT_DLL_EXPORT ServiceNode : public DecoratorNode {
 public:
  /// \brief The Service interface
  class ABT_DLL_EXPORT Service {
   public:
    using UPtr = std::unique_ptr<ServiceNode::Service>;

    Service() = default;
    virtual ~Service() = default;

    // Inits & resets the service
    virtual void onInit(){};

    // Lookup and register data of service
    virtual void lookupAndRegisterData(Blackboard& /*blackboard*/){};

    // Pre update method of the managed service, called before decorated child is ticked
    virtual void preUpdate(){};

    // Post update method of the managed service, called after decorated child was ticked
    virtual void postUpdate(){};
  };

  // Ctor for named node and multiple managed services
  ServiceNode(const std::string& name,
              std::vector<ServiceNode::Service::UPtr> managed_services,
              Extension::UPtr extension_ptr = nullptr);

  // Convenience ctor for only one managed service
  ServiceNode(const std::string& name,
              ServiceNode::Service::UPtr managed_service,
              Extension::UPtr extension_ptr = nullptr);

  virtual ~ServiceNode() override = default;

  // Inits & resets all services and the underlying child
  void onInit() override;

 private:
  // Updates all services before executing the decorated child node
  NodeStatus tick() final;

  // Lookup and register data for managed services
  void lookupAndRegisterData(Blackboard& blackboard) final;

  static std::vector<ServiceNode::Service::UPtr> instantiateInContainer(ServiceNode::Service::UPtr service);

  // The managed services
  std::vector<ServiceNode::Service::UPtr> m_managed_services;
};

}  // namespace yase

#endif  // AGNOSTIC_BEHAVIOR_TREE_DECORATOR_SERVICE_NODE_H
