/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_BEHAVIOR_TREE_DECORATOR_START_AT_NODE_H
#define AGNOSTIC_BEHAVIOR_TREE_DECORATOR_START_AT_NODE_H

#include "agnostic_behavior_tree/decorator_node.h"
#include "agnostic_behavior_tree/utils/condition.h"
#include "agnostic_behavior_tree/utils/dll_export_helper.h"

namespace yase {

// Waits with sub child execution until condition is met first time
class ABT_DLL_EXPORT StartAtNode : public DecoratorNode {
 public:
  explicit StartAtNode(Condition::UPtr condition, Extension::UPtr extension_ptr = nullptr);

  virtual ~StartAtNode() override = default;

 private:
  // Waits until condition is evaluated once to true
  NodeStatus tick() final;

  // onInit() is called is called directly before first evaluation - resets the behavior
  void onInit() final;

  // LookUp data for condition
  void lookupAndRegisterData(Blackboard& blackboard) final;

  // The condition to evaluate
  Condition::UPtr m_condition;

  // Indicates if already evaluated to true
  bool m_done{false};
};

}  // namespace yase

#endif  // AGNOSTIC_BEHAVIOR_TREE_DECORATOR_START_AT_NODE_H
