/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_BEHAVIOR_TREE_ACTIONS_FUNCTOR_ACTION_NODE_H
#define AGNOSTIC_BEHAVIOR_TREE_ACTIONS_FUNCTOR_ACTION_NODE_H

#include "agnostic_behavior_tree/action_node.h"
#include "agnostic_behavior_tree/utils/dll_export_helper.h"

#include <functional>

namespace yase {
// A simple action node which allows to express logic via a lambda.
// This node eases to execute generic logic with a Functor.
class ABT_DLL_EXPORT FunctorActionNode : public ActionNode {
 public:
  using TickFunctor = std::function<NodeStatus()>;

  FunctorActionNode(const std::string& name,
                    FunctorActionNode::TickFunctor tick_functor,
                    Extension::UPtr extension_ptr = nullptr);

  explicit FunctorActionNode(FunctorActionNode::TickFunctor tick_functor, Extension::UPtr extension_ptr = nullptr);

  virtual ~FunctorActionNode() override = default;

  void onInit() override{};

 protected:
  NodeStatus tick() final;

  TickFunctor m_tick_functor;
};

}  // namespace yase

#endif  // AGNOSTIC_BEHAVIOR_TREE_ACTIONS_FUNCTOR_ACTION_NODE_H
