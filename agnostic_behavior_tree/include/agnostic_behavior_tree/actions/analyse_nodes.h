/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_BEHAVIOR_TREE_ACTIONS_ANALYSE_NODES_H
#define AGNOSTIC_BEHAVIOR_TREE_ACTIONS_ANALYSE_NODES_H

#include "agnostic_behavior_tree/action_node.h"
#include "agnostic_behavior_tree/utils/dll_export_helper.h"

namespace yase {
// Returns N Times until it returns preferred NodeStatus. Tracks the method accesses
class ABT_DLL_EXPORT AnalyseNode : public ActionNode {
 public:
  explicit AnalyseNode(const size_t max_repeat_ticks,
                       const NodeStatus return_status = NodeStatus::kSuccess,
                       Extension::UPtr extension_ptr = nullptr);

  AnalyseNode(const std::string& name,
              const size_t max_repeat_ticks,
              const NodeStatus return_status = NodeStatus::kSuccess,
              Extension::UPtr extension_ptr = nullptr);

  virtual ~AnalyseNode() override = default;

  bool isInitialised() const;

  size_t overallTicks() const;

  size_t ticksSinceInit() const;

  size_t onInitCalls() const;

  size_t onTerminateCalls() const;

 protected:
  NodeStatus tick() final;

  void onInit() override;

  void onTerminate() override;

 private:
  const size_t m_max_repeat_ticks;
  const NodeStatus m_return_status;

  // Variables to be reset at onInit
  bool m_initialised{false};
  size_t m_repeat_counter{0};

  // Variables which will not be reset:
  size_t m_overall_ticks{0};
  size_t m_on_init_calls{0};
  size_t m_ticks_since_init{0};
  size_t m_on_terminate_calls{0};
};

// Returns always a Running
class ABT_DLL_EXPORT AlwaysRunning : public AnalyseNode {
 public:
  AlwaysRunning(Extension::UPtr extension_ptr = nullptr);

  virtual ~AlwaysRunning() override = default;
};

// Returns always a Success
class ABT_DLL_EXPORT AlwaysSuccess : public AnalyseNode {
 public:
  AlwaysSuccess(Extension::UPtr extension_ptr = nullptr);

  ~AlwaysSuccess() override = default;
};

// Returns always a Failure
class ABT_DLL_EXPORT AlwaysFailure : public AnalyseNode {
 public:
  AlwaysFailure(Extension::UPtr extension_ptr = nullptr);

  ~AlwaysFailure() override = default;
};

}  // namespace yase

#endif  // AGNOSTIC_BEHAVIOR_TREE_ACTIONS_ANALYSE_NODES_H
