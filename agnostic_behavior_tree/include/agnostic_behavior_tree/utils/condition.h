/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_BEHAVIOR_TREE_UTILS_CONDITION_H
#define AGNOSTIC_BEHAVIOR_TREE_UTILS_CONDITION_H

#include "agnostic_behavior_tree/scoped_blackboard.h"
#include "agnostic_behavior_tree/utils/dll_export_helper.h"

#include <functional>
#include <string>
#include <vector>

namespace yase {

// The Condition interface
//
// Look up required symbols within the Blackboard and evaluate condition
class ABT_DLL_EXPORT Condition {
 public:
  using UPtr = std::unique_ptr<Condition>;

  explicit Condition(const std::string& condition_name);
  virtual ~Condition() = default;
  Condition(const Condition& copy_from) = delete;
  Condition& operator=(const Condition& copy_from) = delete;
  Condition(Condition&&) = default;
  Condition& operator=(Condition&&) = delete;

  // LookUp data necessary for condition evaluation
  virtual void lookupAndRegisterData(const Blackboard& /*blackboard*/);

  // Called before first evaluation - MUST RESET THE CONDITION!
  virtual void onInit() = 0;

  // Evaluate condition
  virtual bool evaluate() = 0;

  const std::string name;
};

class ABT_DLL_EXPORT ConditionFunctor : public Condition {
 public:
  using EvaluationFunction = std::function<bool()>;

  ConditionFunctor(const std::string& condition_name, EvaluationFunction function);

  // Nothing to reset
  void onInit() final;

  // Evaluate condition
  bool evaluate() final;

 private:
  const EvaluationFunction m_function;
};

// Inverts child condition
class ABT_DLL_EXPORT NotCondition : public Condition {
 public:
  explicit NotCondition(Condition::UPtr condition);

  // Look up required symbols for condition evaluation.
  // Executed once when the symbols within the behavior tree are resolved.
  void lookupAndRegisterData(const Blackboard& blackboard) final;

  // Called before first evaluation - MUST RESET THE CONDITION!
  void onInit() final;

  // Evaluate condition
  bool evaluate() final;

 private:
  // Creates condition name with given condition
  static std::string getConditionName(const Condition& condition);

  Condition::UPtr m_condition;
};

// Evaluates multiple conditions --> all must evaluate to true
class ABT_DLL_EXPORT AllOfConditions : public Condition {
 public:
  explicit AllOfConditions(std::vector<Condition::UPtr> conditions);

  // Look up required symbols for condition evaluation.
  // Executed once when the symbols within the behavior tree are resolved.
  void lookupAndRegisterData(const Blackboard& blackboard) final;

  // Called before first evaluation - MUST RESET THE CONDITION!
  void onInit() final;

  // Evaluate condition
  bool evaluate() final;

 private:
  // Creates condition name with given conditions
  static std::string getConditionName(const std::vector<Condition::UPtr>& conditions);

  std::vector<Condition::UPtr> m_conditions;
};

// Evaluates multiple conditions --> any of the conditions must evaluate to true
class ABT_DLL_EXPORT AnyOfConditions : public Condition {
 public:
  explicit AnyOfConditions(std::vector<Condition::UPtr> conditions);

  // Look up required symbols for condition evaluation.
  // Executed once when the symbols within the behavior tree are resolved.
  void lookupAndRegisterData(const Blackboard& blackboard) final;

  // Called before first evaluation - MUST RESET THE CONDITION!
  void onInit() final;

  // Evaluate condition
  bool evaluate() final;

 private:
  // Creates condition name with given conditions
  static std::string getConditionName(const std::vector<Condition::UPtr>& conditions);

  std::vector<Condition::UPtr> m_conditions;
};

}  // namespace yase

#endif  // AGNOSTIC_BEHAVIOR_TREE_UTILS_CONDITION_H
