/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2022
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_BEHAVIOR_TREE_UTILS_TREE_RUN_H
#define AGNOSTIC_BEHAVIOR_TREE_UTILS_TREE_RUN_H

#include "agnostic_behavior_tree/behavior_node.h"
#include "agnostic_behavior_tree/utils/dll_export_helper.h"

namespace yase {

/// \brief Runs tree until Success with clean init/terminate. Will throw at failures
///
/// \note: Be careful to hold the memory since the non const reference does not extend the lifetime:
/// Example: runTree(getTree()); // <-- Can lead to seg faults
ABT_DLL_EXPORT void runTree(yase::BehaviorNode& behavior_to_simulate, const bool print_each_state = false);

/// \brief Runs tree until Success with clean init/terminate. Will throw at failures
ABT_DLL_EXPORT void runTree(yase::BehaviorNode::Ptr behavior_to_simulate, const bool print_each_state = false);

}  // namespace yase

#endif  // AGNOSTIC_BEHAVIOR_TREE_UTILS_TREE_RUN_H