/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_BEHAVIOR_TREE_DECORATORNODE_H
#define AGNOSTIC_BEHAVIOR_TREE_DECORATORNODE_H

#include "agnostic_behavior_tree/behavior_node.h"
#include "agnostic_behavior_tree/utils/dll_export_helper.h"

#include <string>

namespace yase {

// The abstract decorator node
//
// Decorators decorate ONE child behavior
class ABT_DLL_EXPORT DecoratorNode : public BehaviorNode {
 public:
  // Ctor - it is mandatory to name the node
  explicit DecoratorNode(const std::string& name);

  // Ctor with extension and name
  DecoratorNode(const std::string& name, Extension::UPtr extension_ptr);

  virtual ~DecoratorNode() override = default;

  using Ptr = std::shared_ptr<DecoratorNode>;

  // Set child behavior to decorate
  // Only one child is allowed & child must not be null!
  void setChild(BehaviorNode::Ptr child);

  /// \brief Checks if decorator has child
  bool hasChild() const;

  /// \brief Get decorated child reference
  const BehaviorNode& child() const;

  /// \brief Get decorated child reference
  BehaviorNode& child();

  /// \brief Lookup and register data from blackboard and call child as well
  void distributeData() final;

  void onInit() override;

  void onTerminate() override;


 private:
  BehaviorNode::Ptr checkedChild() const;

  // Child behavior to decorate
  BehaviorNode::Ptr m_child_node{nullptr};

  friend class DecoratorManipulator;
};

}  // namespace yase

#endif  // AGNOSTIC_BEHAVIOR_TREE_DECORATORNODE_H
