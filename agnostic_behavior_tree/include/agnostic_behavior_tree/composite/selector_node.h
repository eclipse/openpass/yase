/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_BEHAVIOR_TREE_COMPOSITION_SELECTOR_H
#define AGNOSTIC_BEHAVIOR_TREE_COMPOSITION_SELECTOR_H

#include "agnostic_behavior_tree/composite_node.h"
#include "agnostic_behavior_tree/utils/dll_export_helper.h"

namespace yase {
/// The Selector node
//
// The selector (Priority selector / Fallback) composite tries all children in sequence until one returns
// kRunning or kSuccess. If the child returns kFailure, the next child is ticked.
// If all children fail, the Selector is set also to kFailure.
class SelectorNode : public CompositeNode {
 public:
  explicit SelectorNode(const std::string& name = "Unnamed");


  explicit SelectorNode(Extension::UPtr extension_ptr);


  SelectorNode(const std::string& name, Extension::UPtr extension_ptr);


  virtual ~SelectorNode() override = default;

  void onInit() override;

  /// \brief Get the last running child (will be nullptr if none)
  std::unique_ptr<size_t> getLastRunningChild() const;

 private:
  NodeStatus tick() final;

  // Index of last running child - If null, then no child was active in step before
  std::unique_ptr<size_t> m_last_running_child{nullptr};
};

}  // namespace yase

#endif  // AGNOSTIC_BEHAVIOR_TREE_COMPOSITION_SELECTOR_H
