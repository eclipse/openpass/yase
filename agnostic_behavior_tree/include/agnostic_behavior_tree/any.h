/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_BEHAVIOR_TREE_ANY_H
#define AGNOSTIC_BEHAVIOR_TREE_ANY_H

#if (__cplusplus >= 201703L || defined(_MSVC_LANG) && _MSVC_LANG >= 201703L)

#  ifndef AGNOSTIC_BEHAVIOR_TREE_CPP_17_SUPPORT

#    define AGNOSTIC_BEHAVIOR_TREE_CPP_17_SUPPORT

#  endif

#  include <any>

#else

#  include "third_party/any.hpp"

#endif

namespace yase {

#ifdef AGNOSTIC_BEHAVIOR_TREE_CPP_17_SUPPORT

using std::any;
using std::any_cast;
using std::bad_any_cast;

#else

using linb::any;
using linb::any_cast;
using linb::bad_any_cast;

#endif

}  // namespace yase

#endif  // AGNOSTIC_BEHAVIOR_TREE_ANY_H
