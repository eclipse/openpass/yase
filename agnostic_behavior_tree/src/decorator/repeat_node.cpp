/*******************************************************************************
 * Copyright (c) Robert Bosch GmbH - 2021-2022
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "agnostic_behavior_tree/decorator/repeat_node.h"

namespace yase {

RepeatNTimesNode::RepeatNTimesNode(const size_t repeat_times, Extension::UPtr extension_ptr)
    : DecoratorNode(createNodeName(repeat_times), std::move(extension_ptr)), m_desired_repeats(repeat_times) {}

void RepeatNTimesNode::onInit() {
  m_child_initialised = false;
  m_counter = 0;
}

std::string RepeatNTimesNode::createNodeName(const size_t repeat_times) {
  std::string node_name = ("Repeat[");
  if (repeat_times == 0) {
    node_name.append("Infinite");
  } else {
    node_name.append(std::to_string(repeat_times));
  }
  node_name.append("]Times");
  return node_name;
}

size_t RepeatNTimesNode::repeatTimes() const { return m_desired_repeats; }
NodeStatus RepeatNTimesNode::tick() {
  if (m_counter >= m_desired_repeats && m_desired_repeats != 0) {
    addExecutionInfo();
    return NodeStatus::kSuccess;
  }

  if (!m_child_initialised) {
    child().onInit();
    m_child_initialised = true;
  }

  NodeStatus child_status = child().executeTick();

  if (child_status == NodeStatus::kSuccess) {
    child().onTerminate();
    m_child_initialised = false;
    m_counter++;
    if (m_counter < m_desired_repeats) {
      child_status = NodeStatus::kRunning;
    }
  }

  addExecutionInfo();
  return child_status;
}

void RepeatNTimesNode::addExecutionInfo() {
  std::string info = ("Repeated child behavior ");
  info.append(std::to_string(m_counter));
  if (m_desired_repeats != 0) {
    info.append(" out of ").append(std::to_string(m_desired_repeats));
  }
  info.append(" times");
  executionInfo(info);
}
}  // namespace yase