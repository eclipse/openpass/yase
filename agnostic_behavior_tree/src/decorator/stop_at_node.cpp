/*******************************************************************************
 * Copyright (c) Robert Bosch GmbH - 2021-2022
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "agnostic_behavior_tree/decorator/stop_at_node.h"

namespace yase {

StopAtNode::StopAtNode(Condition::UPtr condition, Extension::UPtr extension_ptr)
    : DecoratorNode(std::string("StopAt[").append(condition->name).append("]"), std::move(extension_ptr)),
      m_condition(std::move(condition)) {}

NodeStatus StopAtNode::tick() {
  if (m_condition->evaluate()) {
    return NodeStatus::kSuccess;
  }
  return child().executeTick();
}

void StopAtNode::lookupAndRegisterData(Blackboard& blackboard) { m_condition->lookupAndRegisterData(blackboard); }

void StopAtNode::onInit() {
  m_condition->onInit();
  DecoratorNode::onInit();
}

}  // namespace yase