/*******************************************************************************
 * Copyright (c) Robert Bosch GmbH - 2021-2022
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "agnostic_behavior_tree/composite/sequence_node.h"

namespace yase {

SequenceNode::SequenceNode(const std::string& name) : SequenceNode(name, nullptr) {}

SequenceNode::SequenceNode(Extension::UPtr extension_ptr) : SequenceNode("Unnamed", std::move(extension_ptr)) {}

SequenceNode::SequenceNode(const std::string& name, Extension::UPtr extension_ptr)
    : CompositeNode(std::string("Sequence[").append(name).append("]"), std::move(extension_ptr)) {}
void SequenceNode::onInit() {
  m_current_child = 0;
  if (childrenCount() != 0) {
    this->child(m_current_child).onInit();
  }
}

size_t SequenceNode::getCurrentChild() const { return m_current_child; }

NodeStatus SequenceNode::tick() {
  // Loop over all children
  for (size_t index = m_current_child; index < childrenCount(); index++) {
    BehaviorNode& child = this->child(index);

    const NodeStatus child_status = child.executeTick();

    switch (child_status) {
      case NodeStatus::kRunning: {
        return child_status;
      }
      case NodeStatus::kFailure: {
        child.onTerminate();
        return child_status;
      }
      case NodeStatus::kSuccess: {
        // Terminate current child and init subsequent one
        child.onTerminate();
        m_current_child++;
        if (m_current_child < childrenCount()) {
          this->child(m_current_child).onInit();
        }
        break;
      }
      default: {
        std::string error_msg = "The child node [";
        error_msg.append(child.name());
        error_msg.append("] returned unknown NodeStatus.");
        throw std::invalid_argument(error_msg);
      }
    }
  }
  return NodeStatus::kSuccess;
}

}  // namespace yase