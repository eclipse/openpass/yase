/*******************************************************************************
 * Copyright (c) Robert Bosch GmbH - 2021-2022
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "agnostic_behavior_tree/composite/selector_node.h"

namespace yase {

SelectorNode::SelectorNode(const std::string& name) : SelectorNode(name, nullptr) {}

SelectorNode::SelectorNode(Extension::UPtr extension_ptr) : SelectorNode("Unnamed", std::move(extension_ptr)) {}

SelectorNode::SelectorNode(const std::string& name, Extension::UPtr extension_ptr)
    : CompositeNode(std::string("Selector[").append(name).append("]"), std::move(extension_ptr)) {}

void SelectorNode::onInit() { m_last_running_child = nullptr; }

std::unique_ptr<size_t> SelectorNode::getLastRunningChild() const {
  if (m_last_running_child == nullptr) {
    return nullptr;
  }
  return std::make_unique<size_t>(*m_last_running_child);
}

NodeStatus SelectorNode::tick() {
  // Loop over all children
  for (size_t index = 0; index < childrenCount(); index++) {
    BehaviorNode& child = this->child(index);

    // Initialize child before executing it if not done before
    if (m_last_running_child == nullptr) {
      child.onInit();
    } else if (index != *m_last_running_child) {
      child.onInit();
    }
    const NodeStatus child_status = child.executeTick();

    switch (child_status) {
      case NodeStatus::kRunning: {
        if (m_last_running_child == nullptr) {
          m_last_running_child = std::make_unique<size_t>(index);
        } else if (index != *m_last_running_child) {
          this->child(*m_last_running_child).onTerminate();
        }
        *m_last_running_child = index;
        return child_status;
      }
      case NodeStatus::kFailure: {
        child.onTerminate();
        if (m_last_running_child != nullptr) {
          if (index == *m_last_running_child) {
            m_last_running_child = nullptr;
          }
        }
        break;
      }
      case NodeStatus::kSuccess: {
        child.onTerminate();
        m_last_running_child = nullptr;
        return child_status;
      }
      default: {
        std::string error_msg = "The child node [";
        error_msg.append(child.name());
        error_msg.append("] returned unknown NodeStatus.");
        throw std::invalid_argument(error_msg);
      }
    }
  }


  if (childrenCount() > 0) {
    // None of the children behavior were able to handle the situation
    return NodeStatus::kFailure;
  } else {
    // None children -> success
    return NodeStatus::kSuccess;
  }
}
}  // namespace yase