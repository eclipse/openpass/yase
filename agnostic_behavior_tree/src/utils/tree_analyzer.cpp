/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "agnostic_behavior_tree/utils/tree_analyzer.h"

#include <agnostic_behavior_tree/behavior_node.h>
#include <agnostic_behavior_tree/utils/visitors.h>

#include <vector>

namespace yase {
// Counts all nodes including the root
size_t countNodes(const BehaviorNode& root_node) {
  size_t count{0};
  auto countFunctor = [&count](const BehaviorNode& /*node*/) mutable { ++count; };
  applyVisitor(root_node, countFunctor);
  return count;
}

}  // namespace yase
