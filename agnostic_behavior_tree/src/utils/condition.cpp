/*******************************************************************************
 * Copyright (c) Robert Bosch GmbH - 2021-2022
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "agnostic_behavior_tree/utils/condition.h"

#include <algorithm>

namespace yase {

Condition::Condition(const std::string& condition_name) : name(condition_name) {}

void Condition::lookupAndRegisterData(const Blackboard&) {}

ConditionFunctor::ConditionFunctor(const std::string& condition_name, ConditionFunctor::EvaluationFunction function)
    : Condition(condition_name), m_function(std::move(function)) {}

void ConditionFunctor::onInit() {}

bool ConditionFunctor::evaluate() { return m_function(); }

NotCondition::NotCondition(Condition::UPtr condition)
    : Condition(getConditionName(*condition)), m_condition(std::move(condition)) {}

void NotCondition::lookupAndRegisterData(const Blackboard& blackboard) {
  m_condition->lookupAndRegisterData(blackboard);
}

void NotCondition::onInit() { m_condition->onInit(); }

bool NotCondition::evaluate() { return !m_condition->evaluate(); }

std::string NotCondition::getConditionName(const Condition& condition) {
  std::string name = "Not[";
  return name.append(condition.name).append("]");
}

AllOfConditions::AllOfConditions(std::vector<Condition::UPtr> conditions)
    : Condition(getConditionName(conditions)), m_conditions(std::move(conditions)) {}

void AllOfConditions::lookupAndRegisterData(const Blackboard& blackboard) {
  for (const auto& condition : m_conditions) {
    condition->lookupAndRegisterData(blackboard);
  }
}

void AllOfConditions::onInit() {
  for (const auto& condition : m_conditions) {
    condition->onInit();
  }
}

bool AllOfConditions::evaluate() {
  return (std::all_of(m_conditions.begin(), m_conditions.end(),
                      [](Condition::UPtr& conidtion) { return conidtion->evaluate(); }));
}

std::string AllOfConditions::getConditionName(const std::vector<Condition::UPtr>& conditions) {
  std::string name = "AllOf[";
  // Add first condition without delimiter in front
  if (conditions.size() > 1) {
    name.append(conditions[0]->name);
  }
  for (size_t idx = 1; idx < conditions.size(); idx++) {
    name.append("&&").append(conditions[idx]->name);
  }
  return name.append("]");
}

AnyOfConditions::AnyOfConditions(std::vector<Condition::UPtr> conditions)
    : Condition(getConditionName(conditions)), m_conditions(std::move(conditions)) {}
void AnyOfConditions::lookupAndRegisterData(const Blackboard& blackboard) {
  for (const auto& condition : m_conditions) {
    condition->lookupAndRegisterData(blackboard);
  }
}

void AnyOfConditions::onInit() {
  for (const auto& condition : m_conditions) {
    condition->onInit();
  }
}

bool AnyOfConditions::evaluate() {
  return (std::any_of(m_conditions.begin(), m_conditions.end(),
                      [](Condition::UPtr& conidtion) { return conidtion->evaluate(); }));
}

std::string AnyOfConditions::getConditionName(const std::vector<Condition::UPtr>& conditions) {
  std::string name = "AnyOf[";
  // Add first condition without delimiter in front
  if (conditions.size() > 1) {
    name.append(conditions[0]->name);
  }
  for (size_t idx = 1; idx < conditions.size(); idx++) {
    name.append("||").append(conditions[idx]->name);
  }
  return name.append("]");
}

}  // namespace yase