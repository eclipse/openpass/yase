# ##############################################################################
# Copyright (c) 2022 Mercedes-Benz Tech Innovation GmbH
#
# This program and the accompanying materials are made available under the terms
# of the Eclipse Public License 2.0 which is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
# ##############################################################################

add_library(agnostic_behavior_tree)
add_library(Yase::agnostic_behavior_tree ALIAS agnostic_behavior_tree)

target_sources(
  agnostic_behavior_tree
  PRIVATE # Basic definitions
          action_node.cpp
          behavior_node.cpp
          composite_node.cpp
          decorator_node.cpp
          scoped_blackboard.cpp
          # Actions
          actions/analyse_nodes.cpp
          actions/functor_action_node.cpp
          # Composite
          composite/chooser_node.cpp
          composite/parallel_node.cpp
          composite/selector_node.cpp
          composite/sequence_node.cpp
          # Decorator
          decorator/constraint_node.cpp
          decorator/data_declaration_node.cpp
          decorator/data_proxy_node.cpp
          decorator/inverter_node.cpp
          decorator/repeat_node.cpp
          decorator/service_node.cpp
          decorator/start_at_node.cpp
          decorator/stop_at_node.cpp
          # Utils
          utils/condition.cpp
          utils/manipulators.cpp
          utils/tree_analyzer.cpp
          utils/tree_print.cpp
          utils/tree_run.cpp
          utils/visitors.cpp
)

target_include_directories(
  agnostic_behavior_tree PUBLIC $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/include> $<INSTALL_INTERFACE:include>
)

target_compile_features(agnostic_behavior_tree PRIVATE cxx_std_17)
