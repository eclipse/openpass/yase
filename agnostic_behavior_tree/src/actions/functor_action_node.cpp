/*******************************************************************************
 * Copyright (c) Robert Bosch GmbH - 2021-2022
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "agnostic_behavior_tree/actions/functor_action_node.h"

namespace yase {

yase::FunctorActionNode::FunctorActionNode(const std::string& name,
                                           yase::FunctorActionNode::TickFunctor tick_functor,
                                           yase::Extension::UPtr extension_ptr)
    : ActionNode(name, std::move(extension_ptr)), m_tick_functor(std::move(tick_functor)) {}

yase::FunctorActionNode::FunctorActionNode(yase::FunctorActionNode::TickFunctor tick_functor,
                                           yase::Extension::UPtr extension_ptr)
    : FunctorActionNode("UnnamedFunctorAction", std::move(tick_functor), std::move(extension_ptr)) {}

yase::NodeStatus yase::FunctorActionNode::tick() { return m_tick_functor(); }

}  // namespace yase