/*******************************************************************************
 * Copyright (c) Robert Bosch GmbH - 2021-2022
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "agnostic_behavior_tree/action_node.h"

namespace yase {

ActionNode::ActionNode(const std::string& name) : ActionNode(name, nullptr) {}

ActionNode::ActionNode(const std::string& name, Extension::UPtr extension_ptr)
    : BehaviorNode(std::string("Action::").append(name), std::move(extension_ptr)) {}

}  // namespace yase