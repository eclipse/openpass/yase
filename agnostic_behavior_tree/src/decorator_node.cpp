/*******************************************************************************
 * Copyright (c) Robert Bosch GmbH - 2021-2022
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "agnostic_behavior_tree/decorator_node.h"

namespace yase {

DecoratorNode::DecoratorNode(const std::string& name) : DecoratorNode(name, nullptr) {}

DecoratorNode::DecoratorNode(const std::string& name, Extension::UPtr extension_ptr)
    : BehaviorNode(std::string("Decorator::").append(name), std::move(extension_ptr)) {}

void DecoratorNode::setChild(BehaviorNode::Ptr child) {
  if (!child) {
    std::string error_msg = "DecoratorNode [";
    error_msg.append(name());
    error_msg.append("]: Added child behavior is NULL.");
    throw std::invalid_argument(error_msg);
  }
  if (child->hasParent()) {
    std::string error_msg = "DecoratorNode [";
    error_msg.append(name());
    error_msg.append("]: Added child behavior is already attached to parent.");
    throw std::invalid_argument(error_msg);
  }
  if (m_child_node) {
    std::string error_msg = "Duplicate child insertion: DecoratorNode [";
    error_msg.append(name());
    error_msg.append("] has already a child assigned.");
    throw std::invalid_argument(error_msg);
  }
  child->setParent(*this);
  m_child_node = child;
}

bool DecoratorNode::hasChild() const { return m_child_node != nullptr; }

const BehaviorNode& DecoratorNode::child() const { return *checkedChild(); }

BehaviorNode& DecoratorNode::child() { return *checkedChild(); }

void DecoratorNode::distributeData() {
  m_blackboard->clearLocal();
  lookupAndRegisterData(*(m_blackboard));
  if (m_child_node != nullptr) {
    m_child_node->distributeData();
  }
}

void DecoratorNode::onInit() {
  if (m_child_node != nullptr) {
    m_child_node->onInit();
  }
}

void DecoratorNode::onTerminate() {
  if (m_child_node != nullptr) {
    m_child_node->onTerminate();
  }
}

BehaviorNode::Ptr DecoratorNode::checkedChild() const {
  if (!hasChild()) {
    std::string exception_msg = "Trying to access the child in DecoratorNode [";
    exception_msg.append(name());
    exception_msg.append("] which is NULL.");
    throw std::runtime_error(exception_msg);
  }
  return m_child_node;
}

}  // namespace yase