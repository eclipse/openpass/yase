
### uml: class diagram
```plantuml
@startuml
package "YASE Behavior Tree Architecture" #DDDDDD {

    note right of BehaviorNode 
        The blackboard allows to access symbols and the 
        extension allows to extend use case specific methods.
    end note

    class BehaviorNode {
        + lookUpSymbol()
        + declareSymbol()
        + virtual tick()
        + blackboard
        + extension
    }
    

    BehaviorNode <|-- Composite
    BehaviorNode <|-- Decorator
    BehaviorNode <|-- Action

    class Decorator {
        + child
        + setChild()
    }
    class Action
    class Composite {
        + addChild()
        + children
    }

    Composite <|-- Sequence
    Composite <|-- Parallel
    Composite <|-- Selector

    class Sequence 
    class Parallel 
    class Selector 

    Decorator <|-- DataDeclarationNode
    Decorator <|-- DataProxyNode
    Decorator <|-- ServiceNode
    Decorator <|-- ConstraintNode
    Decorator <|-- InverterNode
    Decorator <|-- ExecuteUntilFailureNode

    class DataDeclarationNode 
    class DataProxyNode
    class ServiceNode
    class ConstraintNode
    class InverterNode
    class ExecuteUntilFailureNode

    Action <|-- TaskNode
    Action <|-- UnitTestHelper

    class TaskNode 
    class UnitTestHelper 

@enduml
```

